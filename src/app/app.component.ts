import { Component } from '@angular/core';
import { StoreService } from './services/store.service';
import { Store } from './classes/store';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private storeService: StoreService) {
   
  }
   private clearStore(): void {
    this.storeService.clearStore();
  }
}
