export class Store {
  id: number;
  key: any;
  value: string;

  constructor(id: number, key: any,value:string) {
    this.id = id;
    this.key = key;
    this.value=value;
  }
}
