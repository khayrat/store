import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { StoreInputComponent } from './components/store-input/store-input.component';

import { StoreService } from './services/store.service';
import { StoreItemComponent } from './components/store-item/store-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    StoreInputComponent,
    StoreItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    StoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
