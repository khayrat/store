import { Component, OnInit, Input } from '@angular/core';
import { Store } from '../../classes/store';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-store-item',
  templateUrl: './store-item.component.html',
  styleUrls: ['./store-item.component.css']
})
export class StoreItemComponent implements OnInit {

  @Input()
  private store: Store;

  constructor(private storeService: StoreService) { }

  ngOnInit() {
  }

  private removeStore(): void {
    this.storeService.removeStore(this.store.id);
  }

}
