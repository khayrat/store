import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreInputComponent } from './store-input.component';

describe('StoreInputComponent', () => {
  let component: StoreInputComponent;
  let fixture: ComponentFixture<StoreInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
