import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-store-input',
  templateUrl: './store-input.component.html',
  styleUrls: ['./store-input.component.css']
})
export class StoreInputComponent implements OnInit {

  public storeKey: string;
  public storeValue: string;

  constructor(private storeService: StoreService) {
    this.storeKey = '';
    this.storeValue = '';

  }

  ngOnInit() {
  }

  private addStore(): void {
    this.storeService.addStore(this.storeKey,this.storeValue);
    this.storeKey = '';
    this.storeValue = '';

  }
}
