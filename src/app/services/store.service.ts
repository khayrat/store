import { Injectable } from '@angular/core';
import { Store } from '../classes/store';

@Injectable()
export class StoreService {

  private nextId: number;

  constructor() {
  let stores=this.getStores();
    if(stores.length==0)
    {
      this.nextId=0;
    }
    else{
      let maxId=stores[stores.length-1].id;
      this.nextId=maxId+1;
    }
  }

  public addStore(text: string,text2:string): void {
    let store = new Store(this.nextId, text,text2);
    let stores=this.getStores();
    stores.push(store);
    this.setLocalStorageTodos(stores);
    this.nextId++;
  }

  public getStores(): Store[] {
   let localStorageItem=JSON.parse(localStorage.getItem('stores'))
   return localStorageItem == null ? []:localStorageItem.stores;
  }

  public removeStore(id: number): void {
    let stores=this.getStores();
    stores = stores.filter((store)=> store.id != id);
    this.setLocalStorageTodos(stores);

  }
  public clearStore(): void {
    let stores=[];
    this.setLocalStorageTodos(stores);
      this.nextId=0;

  }
  private setLocalStorageTodos(stores:Store[]):void{
    localStorage.setItem('stores',JSON.stringify({stores:stores}));
  }

}
